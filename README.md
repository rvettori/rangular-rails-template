# Rangular Rails Template

This template attempts to integrate the angular and rails transparently without the need to 
duplicate routes in the frontend and backend, using just routes definition in backend.

## Usage

	rails new my_rails_project -m <URL/template.rb>

or existing rails application

	cd my_rails_project
	rake rails:template LOCATION=<PATH/template.rb>

## Using Routes

Use rangular_resources to define the angular routes, ex:
	
	rangular_resoureces :products

Every time you change routes, use the command:
	
	rake js: routes


