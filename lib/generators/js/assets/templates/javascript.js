// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
// Rangular Templates
'use strict';

function <%= class_name %>Controller($scope, $routeParams, $compile, Restangular){
  var <%= class_name %> = Restangular.all(Routes.<%= class_name.pluralize %>());


  // ex: {{routes.root_path()}}
  $scope.routes = Routes;
}
<%= class_name %>Controller.$inject = ['$scope', '$routeParams', '$compile','Restangular'];