=begin

  # Template for Rails 4 Projects.
  Usage:  
    rake rails:template LOCATION=<TEMPLATE_PATH>
  or
    rails new <project_name> -m <TEMPLATE_PATH | TEMPLATE_URL>

--
  * itinializers - mime_types.rb - Mime::Type.register "text/html", :view
  * 
=end

### Add custom gems

gem 'pg'

gem_group :development, :test do
  gem("rspec-rails")
  gem('guard-rspec')
  gem('factory_girl_rails')
  gem('shoulda-matchers')
  gem('jazz_hands')
  # gem("cucumber-rails")
end

gem_group :production do
  gem 'puma'
  gem 'brdinheiro'
  gem 'brstring'
  gem 'brdata'
end

if yes?("Would you like remove turbolinks")
  gsub_file('Gemfile', "gem 'turbolinks'", '')
  gsub_file('app/assets/javascripts/application.js', "//= require turbolinks", '')
  gsub_file('app/views/layouts/application.html.erb', ', "data-turbolinks-track" => true', '')
end

### instaling genms
run 'bundle install'
generate('rspec:install')


### Configure initializers
initializer 'mime_types.rb', 'Mime::Type.register "text/html", :view'


### Routes constraints and angular_resoureces
inject_into_file 'config/routes.rb', before: /(?:.*)Application.routes.draw/ do
  %q{
    ### Constraints for rangularize_resources
    class AngularFormatConstraints
      def self.matches?(request)
        ['json','view'].include?(request.format)
      end
    end

    ### Define for rangularize_resources
    module ActionDispatch::Routing  
      class Mapper
        def rangular_resources(*args, &block)
          constraints AngularFormatConstraints do
            resources *args, &block    
          end
          get '/:angular_route', to: 'application#index' , angular_route: /#{args.first.to_s}.*/  
        end
      end
    end
  } + "\n\n"
end


### Config application.js
inject_into_file 'app/assets/javascripts/application.js', before: /\/\/= require_tree/ do
  %q{
    //= require underscore
    //= require angular
    //= require angular-route
    //= require angular-resource
    //= require restangular
    //= require rails_routes
    //= require rangular_utils
    //= require rangular_app

  }
end

### Override Files
source_url = File.join(File.expand_path(__FILE__),'..') # add repo git
get "#{source_url}/app/assets/javascripts/rangular_app.js"             , 'app/assets/javascripts/rangular_app.js'
get "#{source_url}/app/assets/javascripts/rangular_utils.js"           , 'app/assets/javascripts/rangular_utils.js'
get "#{source_url}/app/controllers/rangular_application_controller.rb" , 'app/controllers/rangular_application_controller.rb'
get "#{source_url}/app/views/layouts/rangular_application.view.erb"    , 'app/views/layouts/rangular_application.view.erb'

### Download 
get "http://underscorejs.org/underscore-min.js"                , 'app/assets/javascripts/underscore-min.js'
get "http://code.angularjs.org/1.2.10/angular-resource.min.js" , 'app/assets/javascripts/angular-resource.min.js'
get "http://code.angularjs.org/1.2.10/angular-route.min.js"    , 'app/assets/javascripts/angular-route.min.js'
get "http://code.angularjs.org/1.2.10/angular.min.js"          , 'app/assets/javascripts/angular.min.js'
get "https://raw2.github.com/mgonto/restangular/master/dist/restangular.min.js", 'app/assets/javascripts/restangular.min.js'

### Generators
# erb
get "#{source_url}/lib/generators/erb/controller/controller_generator.rb", 'lib/generators/erb/controller/controller_generator.rb'
get "#{source_url}/lib/generators/erb/controller/templates/view.html.erb", 'lib/generators/erb/controller/templates/view.html.erb'
# js
get "#{source_url}/lib/generators/js/assets/assets_generator.rb"    , 'lib/generators/js/assets/assets_generator.rb'
get "#{source_url}/lib/generators/js/assets/templates/javascript.js", 'lib/generators/js/assets/templates/javascript.js'


### Configure gems
if yes?("Would you like to install Devise?")
  gem("devise")
  model_name = ask("What would you like the user model to be called? [user]")
  model_name = "user" if model_name.blank?
  generate("devise:install")
  generate("devise", model_name)
end

if yes?('Would you like to initialize git?')
  git :init
  git add: "."
  git commit: %Q{ -m 'Initial commit' }
end



